#pragma once

int load_settings_from_uci(void);
char *get_backend_host(void);
char *get_cacertpath(void);
char *get_clientbundlepath(void);

#define PROCESS_IDENT "door-system"
#define CARD_VALID_URL "%s/cards/is_card_valid/%s/"
#define AUDIT_LOG_URL "%s/cards/log_action/%s/"
#define ADD_CARD_URL "%s/cards/add_card/%s/"
#define COUNTER_CHANGE_URL "%s/cards/counter/%s/"

