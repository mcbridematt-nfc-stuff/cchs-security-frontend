#include <nfc/nfc.h>
#include <freefare.h>
#include <stdio.h>
#include <unistd.h>

#include "format-common.h"
#include "backend-comms.h"
#include <signal.h>

void exit_handler(int);

static nfc_device *device = NULL;

nfc_context *ctx;
nfc_connstring devices[8];
static MifareTag *tags = NULL;

int main(int argc, char **argv) {
	signal(SIGQUIT, exit_handler);
	signal(SIGINT, exit_handler);
	signal(SIGTERM, exit_handler);
	uint8_t connectionAttempts = 0;
	
	printf("Loading settings from UCI\n");
	if(!load_settings_from_uci()) {
		printf("No backend has been configured\n");
		return 1;
	} else {
		printf("Configured backend host: %s\n",get_backend_host());
	}
	nfc_init(&ctx);
	printf("Attempting device connection\n");
	while (device == NULL) {
        size_t device_count = nfc_list_devices(ctx, devices, 8);
        if (device_count <= 0) {
            printf("No NFC readers found\n");
        } else {
            printf("Trying to connect\n");
			device = nfc_open(ctx, devices[0]);
			if (device == NULL) {
				fprintf(stderr,"Unable to open connection to device\n");
			} else {
				break;
			}
        }
        sleep(5);
		connectionAttempts++;
		if (connectionAttempts >= 10) {
			fprintf(stderr,"Timed out trying to find an NFC reader\n");
			exit(1);
		}
    }
	printf("Have NFC reader connection\n");
    while(true) {
        tags = freefare_get_tags(device);
        for (int i = 0; tags[i]; i++) {
        	if (freefare_get_tag_type(tags[i]) == CLASSIC_1K ||
				freefare_get_tag_type(tags[i]) == CLASSIC_4K) {
                    printf("Checking for existing cards... ");
                    char *uid = freefare_get_tag_uid(tags[i]);
                    char keyA[32];
                    size_t encodedLen=0;
                    unsigned int counter = 0;
                    cardAction status = checkIfCardIsValid(uid,&keyA[0],&encodedLen,&counter);
					if (status == CARDACTION_ALLOWED) {
                        printf("Card already provisioned\n");
                        sleep(5);
                    } else if (status == CARDACTION_BLOCKED) {
                        printf("Card is blocked or not provisioned\n");
                        sleep(5);
                    } else if (status == CARDACTION_INVALID ||
						status == CARDACTION_NOTFOUND) {
                        printf("Formatting card\n");
                        char b64KeyA[32],b64KeyB[32];
                        int format_status = format_card(tags[i],uid,&b64KeyA[0],&b64KeyB[0]);
                        if (format_status == 0) {
                            printf("Card formatted\n");
                                sleep(5);
                        } else if (format_status == -1) {
                            printf("Could not add card to database\n");
                            sleep(5);
                        } else if (format_status == -2) {
                            printf("Unable to format card\n");
                            sleep(5);
                        }
                    } else {
						printf("unknown status %d\n", status);
					}
                } else {
					fprintf(stderr,"Not a MiFare 1K or 4K card\n");
				}
        }
    }
}
void exit_handler(int sig_type) {
	printf("Signal %d received, exiting\n",sig_type);
	if (device != NULL) {
		nfc_close(device);
	}
	exit(0);
}
