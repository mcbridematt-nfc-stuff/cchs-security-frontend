#include <uci.h>
#include <string.h>
#include <syslog.h>

#define BACKEND_HOST_LEN 128
char backend_host[BACKEND_HOST_LEN];
#define CAPATHLEN 256
char cacertpath[CAPATHLEN];
char clientbundlepath[CAPATHLEN];


int load_settings_from_uci(void) {
	struct uci_context *uciCtx = NULL;
	struct uci_package *uciPackage = NULL;
	struct uci_element *e;
	int has_host = 0;

	memset(backend_host,0,BACKEND_HOST_LEN);
	memset(cacertpath,0,CAPATHLEN);
	memset(clientbundlepath,0,CAPATHLEN);
	
	uciCtx = uci_alloc_context();
	if (uciCtx == NULL) {
		syslog(LOG_ALERT, "Could not set up UCI context\n");
		return 0;
	}
	uci_load(uciCtx, "access", &uciPackage);
	if (uciPackage == NULL) {
		syslog(LOG_ALERT, "No 'access' section in uci\n");
		return 0;
	}
	uci_foreach_element(&uciPackage->sections, e) {
		struct uci_section *s = uci_to_section(e);
		
		const char *host = uci_lookup_option_string(uciCtx,s,"host");
		if (host == NULL) {
			continue;
		} else {
			strncpy(backend_host,host,128);
			backend_host[127] = '\0';
			has_host = 1;
		}
		const char *s_cacertpath = uci_lookup_option_string(uciCtx,s,"cacertpath");
		if (s_cacertpath != NULL) {
			strncpy(cacertpath,s_cacertpath,256);
			cacertpath[127] = '\0';
		}
		
		const char *s_clientbundle = uci_lookup_option_string(uciCtx,s,"clientbundlepath");
		if (s_clientbundle != NULL) {
			strncpy(clientbundlepath,s_clientbundle,256);
		}
		break;
	}
	uci_free_context(uciCtx);
	return has_host;
}
char *get_backend_host(void) {
	return &backend_host;
}
char *get_cacertpath(void) {
	return &cacertpath;
}
char *get_clientbundlepath(void) {
	return &clientbundlepath;
}
